package com.tms.findher;

import com.tms.findher.Fragments.FragmentList;
import com.tms.findher.Fragments.FragmentStart;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by burak on 07.07.2017.
 */
public class MyPagerAdapterTest {
    @Test
    public void getCount() throws Exception {
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(null);
        assertSame(5, pagerAdapter.getCount());
    }

    @Test
    public void getPosition() throws Exception {
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(null);
        assertSame(MyPagerAdapter.class, pagerAdapter.getClass());
    }



}