package com.tms.findher;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by burak on 30.04.2017.
 */

public class ListAdapter extends ArrayAdapter {

    private String[] androidListViewStrings;
    private Context context;
    private JSONArray jsonArray;
    private boolean black = false;
    private String temp ;
    private JSONObject jsonObj1;
    private JSONArray jsonTempArray;
    MyContentProvider contentProvider;
    public ListAdapter(Activity context, String[] textListView, JSONArray jsonArray) {
        super(context, R.layout.activity_listview, textListView);
        this.androidListViewStrings = textListView;
        this.context = context;
        this.jsonArray = jsonArray;
        //writeToFile("");
        temp = readFromFile();
        try {
            if(!temp.isEmpty()){
                jsonObj1 = new JSONObject(temp);
                jsonTempArray = jsonObj1.getJSONArray("index");
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RecyclerView.ViewHolder holder;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewRow = layoutInflater.inflate(R.layout.activity_listview, viewGroup, false);
        final TextView mNamew = (TextView) viewRow.findViewById(R.id.name);
        mNamew.setTag(i+1);
        TextView mCount = (TextView) viewRow.findViewById(R.id.placeCount);
        TextView mAdress = (TextView) viewRow.findViewById(R.id.anschrift);
        TextView mTel = (TextView) viewRow.findViewById(R.id.telefon);
        final ImageView iv = (ImageView) viewRow.findViewById(R.id.imageView);
        try{iv.setTag(jsonArray.getJSONObject(i));}
        catch(Exception ex) { ex.printStackTrace();}
        iv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            DatabaseHelper contentProvider = new DatabaseHelper(getContext());
            try{
                JSONObject tempJson = (JSONObject) iv.getTag();
                boolean completed = false;
                if(jsonTempArray != null){
                    for(int i = 0; i< jsonTempArray.length(); i++){
                        if(jsonTempArray.getJSONObject(i).getString("name").equals(tempJson.getString("name"))){
                            jsonTempArray.remove(i);
                            writeToFile("");
                            JSONObject obj = new JSONObject();
                            obj.put("index", jsonTempArray);
                            writeToFile(obj.toString());
                            iv.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            completed = true;
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DatabaseHelper.SPALTE_8, false);
                            if(!tempJson.getBoolean("erstellt")){
                                getContext().getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, tempJson.getInt("id")), contentValues, null, null);
                            }
                            else {
                                getContext().getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI2, tempJson.getInt("id")), contentValues, null, null);
                            }
                            break;
                        }
                    }
                }
                if(!completed){
                    tempJson.put("favorite", true);
                    if(!black){
                        iv.setImageResource(R.drawable.ic_favorite_black_24dp);
                        if(!temp.isEmpty()){
                            //Object js = jsonObj1.get("favorite");
                            JSONArray jsonArray =  jsonObj1.getJSONArray("index");
                            jsonArray.put(tempJson);
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("index", jsonArray);
                            String str = jsonObj.toString();
                            writeToFile(str);
                        }
                        else {
                            writeToFile("{index: [" + tempJson.toString() +"]}");
                        }
                        black = true;
                    }
                    else{
                        iv.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        black = false;
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelper.SPALTE_8, true);
                    int a = tempJson.getInt("id");
                    String name = tempJson.getString("name");
                    if(!tempJson.getBoolean("erstellt")){
                        getContext().getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, tempJson.getInt("id")), contentValues, null, null);
                    }
                    else {
                        getContext().getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI2, tempJson.getInt("id")), contentValues, null, null);
                    }
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
            }
        });
        //TextView textView = new TextView(context);
        try {
            String name = jsonArray.getJSONObject(i).getString("name");
            mNamew.setText("Name: " + name);
            mCount.setText("Anzahl der Plätze: " +jsonArray.getJSONObject(i).getString("anzahl_der_plaetze"));
            mAdress.setText("Anschrift: " +jsonArray.getJSONObject(i).getString("anschrift"));
            //mPLZ.setText("PLZ: " +jsonArray.getJSONObject(i).getString("plz"));
            mTel.setText("Telefon: " +jsonArray.getJSONObject(i).getString("telefon"));
            boolean black = false;
            if(jsonTempArray != null){
                for(int j = 0; j < jsonTempArray.length(); j++){
                    String secondName = jsonTempArray.getJSONObject(j).getString("name");
                    if(name.equals(secondName)){
                        black = jsonTempArray.getJSONObject(j).getBoolean("favorite");
                        break;
                    }
                }
            }
            if(black) {
                iv.setImageResource(R.drawable.ic_favorite_black_24dp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return viewRow;
    }

    private String readFromFile() {
        String ret = "";
        try {
            InputStream inputStream = getContext().openFileInput("config.json");
            if (inputStream != null){
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().openFileOutput("config.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
