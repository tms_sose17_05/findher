package com.tms.findher;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class MyContentProvider extends ContentProvider {

    private DatabaseHelper databaseHelper;

    private static final int STELLEN_ONLINE = 1;
    private static final int STELLEN_ERSTELLT = 2;
    private static final int EINZELNE_STELLE_ONLINE = 3;
    private static final int EINZELNE_STELLE_ERSTELLT= 4;

    private static final String AUTHORITY = "com.tms.findher.contentprovider";
    public static final Uri CONTENT_URI1 = Uri.parse("content://" + AUTHORITY + "/online");
    public static final Uri CONTENT_URI2 = Uri.parse("content://" + AUTHORITY + "/erstellt");

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "online", STELLEN_ONLINE);
        uriMatcher.addURI(AUTHORITY, "online/#", EINZELNE_STELLE_ONLINE);
        uriMatcher.addURI(AUTHORITY, "erstellt", STELLEN_ERSTELLT);
        uriMatcher.addURI(AUTHORITY, "erstellt/#", EINZELNE_STELLE_ERSTELLT);
    }

    public MyContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriMatcher.match(uri)){
            case EINZELNE_STELLE_ONLINE:
                String id1 = uri.getLastPathSegment();
                rowsDeleted = db.delete(DatabaseHelper.TABLE_1, DatabaseHelper.SPALTE_1 + "=" + id1, null);
                break;
            case EINZELNE_STELLE_ERSTELLT:
                String id2 = uri.getLastPathSegment();
                rowsDeleted = db.delete(DatabaseHelper.TABLE_2, DatabaseHelper.SPALTE_1 + "=" + id2, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case STELLEN_ONLINE:
                return "vnd.android.cursor.dir/vnd.com.tms.findher.contentprovider.online";
            case EINZELNE_STELLE_ONLINE:
                return "vnd.android.cursor.item/vnd.com.tms.findher.contentprovider.online";
            case STELLEN_ERSTELLT:
                return "vnd.android.cursor.dir/vnd.com.tms.findher.contentprovider.erstellt";
            case EINZELNE_STELLE_ERSTELLT:
                return "vnd.android.cursor.item/vnd.com.tms.findher.contentprovider.erstellt";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Uri _uri = null;
        switch (uriMatcher.match(uri)) {
            case STELLEN_ONLINE:
                long id1 = db.insert(DatabaseHelper.TABLE_1, null, values);
                if(id1 > 0){
                    _uri = ContentUris.withAppendedId(CONTENT_URI1, id1);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            case STELLEN_ERSTELLT:
                long id2 = db.insert(DatabaseHelper.TABLE_2, null, values);
                if(id2 > 0){
                    _uri = ContentUris.withAppendedId(CONTENT_URI2, id2);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            default: throw new SQLException("Failed to insert row into " + uri);
        }
        return _uri;
    }

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)){
            case STELLEN_ONLINE:
                queryBuilder.setTables(DatabaseHelper.TABLE_1);
                break;
            case EINZELNE_STELLE_ONLINE:
                String id1 = uri.getLastPathSegment();
                queryBuilder.setTables(DatabaseHelper.TABLE_1);
                queryBuilder.appendWhere(DatabaseHelper.SPALTE_1 + "=" + id1);
                break;
            case STELLEN_ERSTELLT:
                queryBuilder.setTables(DatabaseHelper.TABLE_2);
                break;
            case EINZELNE_STELLE_ERSTELLT:
                String id2 = uri.getLastPathSegment();
                queryBuilder.setTables(DatabaseHelper.TABLE_2);
                queryBuilder.appendWhere(DatabaseHelper.SPALTE_1 + "=" + id2);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriMatcher.match(uri)){
            case EINZELNE_STELLE_ONLINE:
                String id1 = uri.getLastPathSegment();
                rowsUpdated = db.update(DatabaseHelper.TABLE_1, values, DatabaseHelper.SPALTE_1 + "=" + id1, null);
                break;
            case EINZELNE_STELLE_ERSTELLT:
                String id2 = uri.getLastPathSegment();
                rowsUpdated = db.update(DatabaseHelper.TABLE_2, values, DatabaseHelper.SPALTE_1 + "=" + id2, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
