package com.tms.findher;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tms.findher.Fragments.FragmentCreate;
import com.tms.findher.Fragments.FragmentFavorites;
import com.tms.findher.Fragments.FragmentMap;
import com.tms.findher.Fragments.FragmentStart;
import com.tms.findher.Fragments.FragmentList;

public class MyPagerAdapter extends FragmentPagerAdapter {


    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    private Fragment f = null;
    private Fragment fS =null;

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                f = new FragmentStart();
                break;
            case 1:
                f = new FragmentList();
                break;
            case 2:
                f = new FragmentMap();
                break;
            case 3:
                f = new FragmentFavorites();
                break;
            case 4:
                f = new FragmentCreate();
                break;
        }
        return f;
    }

    @Override
    public int getCount() { // Return the number of pages
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) { // Set the tab text
        if (position == 0) {
            return "Start";
        }
        if (position == 1) {
            return "Liste";
        }
        if (position == 2) {
            return "Karte";
        }
        if (position == 3) {
            return "Favoriten";
        }
        if (position == 4) {
            return "Erstellen";
        }
        return getPageTitle(position);
    }
}
