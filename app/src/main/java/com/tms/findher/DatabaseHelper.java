package com.tms.findher;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "findher.db";
    public static final String TABLE_1 = "Stellen_Online";
    public static final String TABLE_2 = "Stellen_Erstellt";
    public static final String SPALTE_1 = "_id";
    public static final String SPALTE_2 = "Name";
    public static final String SPALTE_3 = "Plaetze";
    public static final String SPALTE_4 = "Anschrift";
    public static final String SPALTE_5 = "Telefon";
    public static final String SPALTE_6 = "Latitude";
    public static final String SPALTE_7 = "Longitude";
    public static final String SPALTE_8 = "Favorit";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "CREATE TABLE "+TABLE_1+" ("+
                        SPALTE_1+" INTEGER PRIMARY KEY, "+
                        SPALTE_2+" TEXT, "+
                        SPALTE_3+" INTEGER, "+
                        SPALTE_4+" TEXT, "+
                        SPALTE_5+" TEXT, "+
                        SPALTE_6+" REAL, "+
                        SPALTE_7+" REAL, "+
                        SPALTE_8+" BOOLEAN)"
        );
        sqLiteDatabase.execSQL(
                "CREATE TABLE "+TABLE_2+" ("+
                        SPALTE_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
                        SPALTE_2+" TEXT, "+
                        SPALTE_3+" INTEGER, "+
                        SPALTE_4+" TEXT, "+
                        SPALTE_5+" TEXT, "+
                        SPALTE_6+" REAL, "+
                        SPALTE_7+" REAL, "+
                        SPALTE_8+" BOOLEAN)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_2);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_1);
        onCreate(sqLiteDatabase);
    }
}