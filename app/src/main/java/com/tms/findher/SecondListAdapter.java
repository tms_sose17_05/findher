package com.tms.findher;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tms.findher.Fragments.FragmentList;

import org.json.JSONArray;

/**
 * Created by burak on 02.07.2017.
 */

public class SecondListAdapter extends ArrayAdapter {

    private String[] androidListViewStrings;
    private Context context;
    private JSONArray jsonArray;

    public SecondListAdapter(Context context, String[] textListView, JSONArray jsonArray) {
        super(context, R.layout.fragment_favorites, textListView);
        this.androidListViewStrings = textListView;
        this.context = context;
        this.jsonArray = jsonArray;
    }
    @Override
    public int getCount() {
        return jsonArray.length();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup container) {
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewRow = layoutInflater.inflate(R.layout.fragment_favorites, container,
                false);
        TextView mNamew = (TextView) viewRow.findViewById(R.id.name);
        TextView mCount = (TextView) viewRow.findViewById(R.id.placeCount);
        TextView mAdress = (TextView) viewRow.findViewById(R.id.anschrift);
        TextView mTel = (TextView) viewRow.findViewById(R.id.telefon);
        if(jsonArray != null){
            try {
                mNamew.setText("Name: " + jsonArray.getJSONObject(i).getString("name"));
                mCount.setText("Anzahl der Plätze: " +jsonArray.getJSONObject(i).getString("anzahl_der_plaetze"));
                mAdress.setText("Anschrift: " +jsonArray.getJSONObject(i).getString("anschrift"));
                //mPLZ.setText("PLZ: " +jsonArray.getJSONObject(i).getString("plz"));
                mTel.setText("Telefon: " +jsonArray.getJSONObject(i).getString("telefon"));
            }
            catch (Exception ex){

            }
        }
        else {
            mNamew.setVisibility(View.GONE);
        }

        return viewRow;
    }
}
