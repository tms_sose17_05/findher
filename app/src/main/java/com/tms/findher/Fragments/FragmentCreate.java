package com.tms.findher.Fragments;

import android.content.ContentValues;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.tms.findher.DatabaseHelper;
import com.tms.findher.MyContentProvider;
import com.tms.findher.R;

import java.io.IOException;
import java.util.List;

public class FragmentCreate extends Fragment {

    EditText name, anschrift, telefon, plaetze;

    public FragmentCreate() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_create, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        name = (EditText)view.findViewById(R.id.name);
        anschrift = (EditText)view.findViewById(R.id.anschrift);
        telefon = (EditText)view.findViewById(R.id.telefon);
        plaetze = (EditText)view.findViewById(R.id.plaetze);

        Button speichern = (Button)view.findViewById(R.id.speichern);
        speichern.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                speichern(v);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void speichern(View v){
        String anschriftText = anschrift.getText().toString();

        LatLng latLng = getLocationFromAddress(anschriftText);

        if(latLng != null){
            String nameText = name.getText().toString();
            String telefonText= telefon.getText().toString();
            int plaetzeInt = Integer.parseInt(plaetze.getText().toString());
            double latitude = latLng.latitude;
            double longitude = latLng.longitude;

            ContentValues contentValues = new ContentValues();

            contentValues.put(DatabaseHelper.SPALTE_2, nameText);
            contentValues.put(DatabaseHelper.SPALTE_3, plaetzeInt);
            contentValues.put(DatabaseHelper.SPALTE_4, anschriftText);
            contentValues.put(DatabaseHelper.SPALTE_5, telefonText);
            contentValues.put(DatabaseHelper.SPALTE_6, latitude);
            contentValues.put(DatabaseHelper.SPALTE_7, longitude);
            contentValues.put(DatabaseHelper.SPALTE_8, false);

            getContext().getContentResolver().insert(MyContentProvider.CONTENT_URI2, contentValues);

            Toast.makeText(getContext(), R.string.gespeichert, Toast.LENGTH_SHORT).show();

            name.setText("");
            anschrift.setText("");
            telefon.setText("");
            plaetze.setText("");
        }
    }

    public LatLng getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(getContext());
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null || address.size() == 0) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p1;
    }
}
