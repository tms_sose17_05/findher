package com.tms.findher.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tms.findher.DatabaseHelper;
import com.tms.findher.ListAdapter;
import com.tms.findher.MyContentProvider;
import com.tms.findher.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FragmentList extends Fragment {
    private View v;
    private JSONObject jsonOutput = new JSONObject();
    private JSONArray arrayOutput;
    private JSONArray sortedJsonArray;
    private TextView tv;
    ListAdapter androidListAdapter;
    ListView androidListView;
    RetrieveFeedTask task;
    public static String TEST;

    public FragmentList(){

    }
    private JSONArray getMoms(){
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();
        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_2, DatabaseHelper.SPALTE_3, DatabaseHelper.SPALTE_4, DatabaseHelper.SPALTE_5, DatabaseHelper.SPALTE_6, DatabaseHelper.SPALTE_7, DatabaseHelper.SPALTE_8};
        Cursor cursor = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI1, projection, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for(int i = 0; i < cursor.getCount(); i++){
                try{
                    obj.put("id", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_1)));
                    obj.put("name", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_2)));
                    obj.put("anzahl_der_plaetze", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_3)));
                    obj.put("anschrift", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_4)));
                    obj.put("telefon", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_5)));
                    obj.put("favorite", false);
                    obj.put("erstellt", false);
                    arr.put(obj);
                    obj = new JSONObject();
                    cursor.moveToNext();
                }
                catch (Exception ex) {
                    ex.printStackTrace();}
            }
            cursor.close();
        }
        Cursor cursor2 = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI2, projection, null, null, null);
        if (cursor2 != null){
            cursor2.moveToFirst();
            for(int i = 0; i < cursor2.getCount(); i++){
                try
                {
                    obj.put("id", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_1)));
                    obj.put("name", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_2)));
                    obj.put("anzahl_der_plaetze", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_3)));
                    obj.put("anschrift", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_4)));
                    obj.put("telefon", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_5)));
                    obj.put("favorite", false);
                    obj.put("erstellt", true);
                    arr.put(obj);
                    obj = new JSONObject();
                    cursor2.moveToNext();
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
            }
            cursor2.close();
        }
        return arr;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_start, container, false);
        androidListView = (ListView)v.findViewById(R.id.listView);
        arrayOutput = getMoms();
        androidListAdapter = new ListAdapter(FragmentList.this.getActivity(), new String[]{}, arrayOutput);
        androidListView.setAdapter(androidListAdapter);
        return v;
    }

    public FragmentList getPosition(){
        return new FragmentList();
    }

    private class RetrieveFeedTask extends AsyncTask<String, Void, JSONObject> {
        private Exception exception;
        private TextView textView;
        public RetrieveFeedTask(TextView text){
            this.textView = text;
        }
        JSONObject json = new JSONObject();

        @Override
        protected JSONObject doInBackground(String... urls) {
            try
            {
                InputStream is = new URL(urls[0]).openStream();
                try
                {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                    String jsonText = readAll(rd);
                    JSONObject json = new JSONObject(jsonText);
                    jsonOutput = json;
                    return json;
                }
                finally
                {
                    is.close();
                }
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }
        private  String readAll(Reader rd) throws IOException {
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            return sb.toString();
        }
        @Override
        protected void onPostExecute(JSONObject feed) {
            super.onPostExecute(feed);
            try{
                jsonOutput = feed;
                arrayOutput = feed.getJSONArray("index");
                TEST = arrayOutput.getJSONObject(1).getString("name");
            }
            catch (Exception ex){ ex.printStackTrace();}
            androidListAdapter = new ListAdapter(FragmentList.this.getActivity(), new String[]{}, arrayOutput);
            androidListView.setAdapter(androidListAdapter);
        }
    }
}
