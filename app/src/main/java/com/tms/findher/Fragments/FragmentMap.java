package com.tms.findher.Fragments;

import android.Manifest;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tms.findher.DatabaseHelper;
import com.tms.findher.MapWrapperLayout;
import com.tms.findher.MyContentProvider;
import com.tms.findher.OnInfoWindowElemTouchListener;
import com.tms.findher.R;
import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;


public class FragmentMap extends Fragment implements OnMapReadyCallback{
    private ViewGroup infoWindow;
    private TextView infoTitle;
    private TextView infoSnippet;
    private ImageButton infoButtonPhone;
    private ImageButton infoButtonFavorite;
    private OnInfoWindowElemTouchListener infoButtonListener;
    private OnInfoWindowElemTouchListener infoButtonListener2;
    private GoogleMap map;
    private MapView mMapView;
    private MapWrapperLayout mapWrapperLayout;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 1;
    HashMap<Integer, Marker> markerList = new HashMap<Integer, Marker>();
    HashMap<Integer, Marker> markerListErstellt = new HashMap<Integer, Marker>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissions();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        checkPermissions();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mapWrapperLayout = (MapWrapperLayout)rootView.findViewById(R.id.map_relative_layout);
        mMapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void checkPermissions(){

        if(Build.VERSION.SDK_INT >= 23){
            if ( ContextCompat.checkSelfPermission( getContext(), Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
                if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)){
                    Toast.makeText(getContext(), R.string.locationErklaerung, Toast.LENGTH_LONG).show();
                }
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch(requestCode){
            case REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getContext(), R.string.locationDeaktiviert, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private String getAddressFromLatLng( LatLng latLng ) {
        Geocoder geocoder = new Geocoder(getContext());

        String address = "";
        try {
            address = geocoder
                    .getFromLocation( latLng.latitude, latLng.longitude, 1 )
                    .get( 0 ).getAddressLine( 0 );
        } catch (IOException e ) {
        }
        return address;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checkPermissions();
        }

        setUpMap();
    }

    private void setUpMap() {
        // MapWrapperLayout initialization
        // 39 - default marker height
        // 20 - offset between the default InfoWindow bottom edge and it's content bottom edge
        this.mapWrapperLayout.init(map, getPixelsFromDp(getContext(), 39 + 20));

        // We want to reuse the info window for all the markers,
        // so let's create only one class member instance
        this.infoWindow = (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.popup, null);
        this.infoTitle = (TextView)infoWindow.findViewById(R.id.title);
        this.infoSnippet = (TextView)infoWindow.findViewById(R.id.snippet);
        this.infoButtonPhone = (ImageButton)infoWindow.findViewById(R.id.phone);
        this.infoButtonFavorite = (ImageButton)infoWindow.findViewById(R.id.favorite);

        // Setting custom OnTouchListener which deals with the pressed state
        // so it shows up
        this.infoButtonListener = new OnInfoWindowElemTouchListener(infoButtonPhone/*,
                ResourcesCompat.getDrawable(getResources(), R.drawable.ic_phone_black_24dp, null),
                ResourcesCompat.getDrawable(getResources(), R.drawable.ic_phone_black_24dp,null)*/)
        {
            @Override
            protected void onClickConfirmed(View v, Marker marker) {

                for(int id: markerList.keySet()){
                    if(markerList.get(id).getId().equals(marker.getId())) {
                        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_5};
                        Cursor cursor = getContext().getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), projection, null, null, null);
                        String tel = "";
                        if(cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                tel = cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_5));
                            }
                        }
                        Intent callIntent = new Intent(Intent.ACTION_DIAL); //Alternative ACTION_CALL, braucht Permission, Anruf wird automatisch gestartet
                        callIntent.setData(Uri.parse("tel:" + tel)); //hier muss Nummer automatisch eingetragen werden
                        getContext().startActivity(callIntent);
                        return;
                    }
                }
                for(int id: markerListErstellt.keySet()){
                    if(markerListErstellt.get(id).getId().equals(marker.getId())){
                        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_5};
                        Cursor cursor = getContext().getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), projection, null, null, null);
                        String tel = "";
                        if(cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                tel = cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_5));
                            }
                        }
                        Intent callIntent = new Intent(Intent.ACTION_DIAL); //Alternative ACTION_CALL, braucht Permission, Anruf wird automatisch gestartet
                        callIntent.setData(Uri.parse("tel:"+tel)); //hier muss Nummer automatisch eingetragen werden
                        getContext().startActivity(callIntent);
                        return;
                    }
                }
            }
        };
        this.infoButtonPhone.setOnTouchListener(infoButtonListener);

        this.infoButtonListener2 = new OnInfoWindowElemTouchListener(infoButtonFavorite/*,
                ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_black_24dp, null),
                ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_black_24dp, null)*/)
        {
            @Override
            protected void onClickConfirmed(View v, Marker marker) {
                Toast.makeText(getContext(), marker.getTitle() + "'s button clicked!", Toast.LENGTH_SHORT).show();

                for(int id: markerList.keySet()){
                    if(markerList.get(id).getId().equals(marker.getId())) {
                        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_8};
                        Cursor cursor = getContext().getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), projection, null, null, null);
                        Boolean favorite = false;
                        if(cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                favorite = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0;
                            }
                        }
                        if(!favorite){
                            this.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_black_24dp, null));
                            Toast.makeText(getContext(), marker.getTitle() + " zu den Favoriten hinzugefügt.", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            this.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_border_black_24dp, null));
                            Toast.makeText(getContext(), marker.getTitle() + " aus den Favoriten entfernt.", Toast.LENGTH_SHORT).show();
                        }
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHelper.SPALTE_8, !favorite);
                        getContext().getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), contentValues, null, null);
                        return;
                    }
                }
                for(int id: markerListErstellt.keySet()){
                    if(markerListErstellt.get(id).getId().equals(marker.getId())){
                        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_8};
                        Cursor cursor = getContext().getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), projection, null, null, null);
                        Boolean favorite = false;
                        if(cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                favorite = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0;
                            }
                        }
                        if(!favorite){
                            v.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_black_24dp, null));
                            Toast.makeText(getContext(), marker.getTitle() + " zu den Favoriten hinzugefügt.", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            v.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_border_black_24dp, null));
                            Toast.makeText(getContext(), marker.getTitle() + " aus den Favoriten entfernt.", Toast.LENGTH_SHORT).show();
                        }
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHelper.SPALTE_8, !favorite);
                        getContext().getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), contentValues, null, null);
                        return;
                    }
                }
            }
        };
        this.infoButtonFavorite.setOnTouchListener(infoButtonListener2);

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Setting up the infoWindow with current's marker info
                infoTitle.setText(marker.getTitle());
                infoSnippet.setText(marker.getSnippet());
                infoButtonListener.setMarker(marker);
                infoButtonListener2.setMarker(marker);
                for(int id: markerList.keySet()) {
                    if (markerList.get(id).getId().equals(marker.getId())) {
                        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_8};
                        Cursor cursor = getContext().getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, id), projection, null, null, null);
                        Boolean favorite = false;
                        if (cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                favorite = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0;
                            }
                        }
                        if(favorite) infoButtonListener2.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_black_24dp, null));
                        else infoButtonListener2.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_border_black_24dp, null));
                    }
                }
                for(int id: markerList.keySet()) {
                    if (markerList.get(id).getId().equals(marker.getId())) {
                        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_8};
                        Cursor cursor = getContext().getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI2, id), projection, null, null, null);
                        Boolean favorite = false;
                        if (cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                favorite = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0;
                            }
                        }
                        if(favorite) infoButtonListener2.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_black_24dp, null));
                        else infoButtonListener2.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_favorite_border_black_24dp, null));
                    }
                }
                mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);
                return infoWindow;
            }
        });


        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setAllGesturesEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(52.441389,13.284069))
                    .zoom(11)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        else{ checkPermissions(); }

        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_2, DatabaseHelper.SPALTE_3, DatabaseHelper.SPALTE_4, DatabaseHelper.SPALTE_5, DatabaseHelper.SPALTE_6, DatabaseHelper.SPALTE_7, DatabaseHelper.SPALTE_8};
        Cursor cursor = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI1, projection, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();
            for(int i = 0; i < cursor.getCount(); i++){
                LatLng ort = new LatLng(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.SPALTE_6)), cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.SPALTE_7)));
                MarkerOptions m = new MarkerOptions()
                        .position(ort)
                        .title(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_2)))
                        .snippet(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_4))+"\nAnzahl Plätze: "
                                +cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_3))+"\nTelefonnr.: "
                                +cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_5)));
                markerList.put(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_1)), map.addMarker(m));
                cursor.moveToNext();
            }
            cursor.close();
        }
        String[] projection2 = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_2, DatabaseHelper.SPALTE_3, DatabaseHelper.SPALTE_4, DatabaseHelper.SPALTE_5, DatabaseHelper.SPALTE_6, DatabaseHelper.SPALTE_7, DatabaseHelper.SPALTE_8};
        Cursor cursor2 = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI2, projection, null, null, null);
        if (cursor2 != null){
            cursor2.moveToFirst();
            for(int i = 0; i < cursor2.getCount(); i++){
                LatLng ort = new LatLng(cursor2.getDouble(cursor2.getColumnIndex(DatabaseHelper.SPALTE_6)), cursor2.getDouble(cursor2.getColumnIndex(DatabaseHelper.SPALTE_7)));
                MarkerOptions m = new MarkerOptions()
                        .position(ort)
                        .title(cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_2)))
                        .snippet(cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_4))+"\nAnzahl Plätze: "
                                +cursor2.getInt(cursor2.getColumnIndex(DatabaseHelper.SPALTE_3))+"\nTelefonnr.: "
                                +cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_5)));
                markerListErstellt.put(cursor2.getInt(cursor2.getColumnIndex(DatabaseHelper.SPALTE_1)), map.addMarker(m));
                cursor2.moveToNext();
            }
            cursor2.close();
        }
    }

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }
}
