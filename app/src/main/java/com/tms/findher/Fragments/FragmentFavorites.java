package com.tms.findher.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.tms.findher.DatabaseHelper;
import com.tms.findher.ListAdapter;
import com.tms.findher.MyContentProvider;
import com.tms.findher.R;
import com.tms.findher.SecondListAdapter;
import com.tms.findher.Utilities.Globals;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class FragmentFavorites extends Fragment {
    private View v;
    TextView titleSearch;
    public int pos =0;
    SecondListAdapter androidListAdapter;
    private ListView androidListView;
    public FragmentFavorites(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_start1, container, false);
        androidListView = (ListView)v.findViewById(R.id.listView);
        JSONArray jsonArray = getFavorites();
        if(jsonArray == null){
            androidListAdapter = new SecondListAdapter(FragmentFavorites.this.getActivity(), new String[]{}, new JSONArray());
        }
        else{
            androidListAdapter = new SecondListAdapter(FragmentFavorites.this.getActivity(), new String[]{}, jsonArray);
        }
        androidListView.setAdapter(androidListAdapter);

        return v;
    }
    private JSONArray getFavorites(){
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();

        String[] projection = {DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_2, DatabaseHelper.SPALTE_3, DatabaseHelper.SPALTE_4, DatabaseHelper.SPALTE_5, DatabaseHelper.SPALTE_6, DatabaseHelper.SPALTE_7, DatabaseHelper.SPALTE_8};
        Cursor cursor = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI1, projection, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();
            for(int i = 0; i < cursor.getCount(); i++){
                try{
                    if(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0){
                        obj.put("id", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_1)));
                        obj.put("name", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_2)));
                        obj.put("anzahl_der_plaetze", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_3)));
                        obj.put("anschrift", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_4)));
                        obj.put("telefon", cursor.getString(cursor.getColumnIndex(DatabaseHelper.SPALTE_5)));
                        obj.put("erstellt", false);
                        arr.put(obj);
                        obj = new JSONObject();
                    }

                    cursor.moveToNext();
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }

            }
            cursor.close();
        }
        Cursor cursor2 = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI2, projection, null, null, null);
        if (cursor2 != null){
            cursor2.moveToFirst();
            for(int i = 0; i < cursor2.getCount(); i++){
                try{
                    if(cursor2.getInt(cursor2.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0) {
                        obj.put("id", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_1)));
                        obj.put("name", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_2)));
                        obj.put("anzahl_der_plaetze", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_3)));
                        obj.put("anschrift", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_4)));
                        obj.put("telefon", cursor2.getString(cursor2.getColumnIndex(DatabaseHelper.SPALTE_5)));
                        obj.put("erstellt", true);
                        arr.put(obj);
                        obj = new JSONObject();
                    }

                    cursor2.moveToNext();
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }

            }
            cursor2.close();
        }
        return arr;
    }
    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = getContext().openFileInput("config.json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

}
