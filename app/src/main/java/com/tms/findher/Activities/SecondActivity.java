package com.tms.findher.Activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.tms.findher.ListAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.tms.findher.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SecondActivity extends AppCompatActivity {
    JSONArray arrayOutput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        String profile = getIntent().getStringExtra("profile");
        try {
            JSONObject profileJSON = new JSONObject(profile);
            arrayOutput = profileJSON.getJSONArray("index");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray sortedJsonArray = new JSONArray();
        List<JSONObject> jsonList = new ArrayList<JSONObject>();
        for (int i = 0; i < arrayOutput.length(); i++) {
            try {
                jsonList.add(arrayOutput.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Collections.sort( jsonList, new Comparator<JSONObject>() {

            public int compare(JSONObject a, JSONObject b) {
            String valA = new String();
            String valB = new String();

            try {
                valA = (String) a.get("anzahl_der_plaetze");
                valB = (String) b.get("anzahl_der_plaetze");
            }
            catch (JSONException e) {
                //do something
            }
            return valA.compareTo(valB);
            }
        });

        for (int i = 0; i < arrayOutput.length(); i++) {
            sortedJsonArray.put(jsonList.get(i));
        }
        ListAdapter androidListAdapter = new ListAdapter(this, new String[]{}, sortedJsonArray);
        ListView androidListView = (ListView) findViewById(R.id.listView);

        LayoutInflater inflater = getLayoutInflater();
        View header = inflater.inflate(R.layout.header, androidListView, false);
        androidListView.addHeaderView(header, null, false);
        androidListView.setAdapter(androidListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) SecondActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(SecondActivity.this.getComponentName()));
        }

        return super.onCreateOptionsMenu(menu);
    }

}
