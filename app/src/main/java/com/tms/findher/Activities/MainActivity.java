package com.tms.findher;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tms.findher.QuickSearchData.LogQuickSearch;
import com.tms.findher.QuickSearchData.LogQuickSearchAdapter;
import com.tms.findher.SearchAndGet.SearchItem;
import com.tms.findher.SearchListView.Item;
import com.tms.findher.SearchListView.SearchAdapter;
import com.tms.findher.Utilities.InitiateSearch;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TextView tv;
    private JSONObject jsonOutput;
    private Resources res;
    private InitiateSearch initiateSearch;
    private Toolbar toolbar;
    private TabLayout tabs;
    private View line_divider;
    private View toolbar_shadow;
    private RelativeLayout view_search;
    private CardView card_search;
    private ImageView image_search_back;
    private ImageView clearSearch;
    private EditText edit_text_search;
    private ListView listView;
    private ListView listContainer;
    private LogQuickSearchAdapter logQuickSearchAdapter;
    private Set<String> set;
    private ArrayList<Item> mItem;
    private SearchItem mNameSearch;
    private SearchAdapter searchAdapter;
    private ProgressBar marker_progress;
    private String brand;
    private AsyncTask<String, String, String> mAsyncTask;

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res = this.getResources();
        tv = new TextView(this);
        jsonOutput = new JSONObject();
        initiateSearch = new InitiateSearch();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabs = (TabLayout) findViewById(R.id.tabs);
        view_search = (RelativeLayout) findViewById(R.id.view_search);
        line_divider = findViewById(R.id.line_divider);
        toolbar_shadow = findViewById(R.id.toolbar_shadow);
        edit_text_search = (EditText) findViewById(R.id.edit_text_search);
        card_search = (CardView) findViewById(R.id.card_search);
        image_search_back = (ImageView) findViewById(R.id.image_search_back);
        clearSearch = (ImageView) findViewById(R.id.clearSearch);
        listView = (ListView) findViewById(R.id.listView);
        listContainer = (ListView) findViewById(R.id.listContainer);
        marker_progress = (ProgressBar) findViewById(R.id.marker_progress);
        marker_progress.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FFFFFF"),//Pink color
            android.graphics.PorterDuff.Mode.MULTIPLY);
        logQuickSearchAdapter = new LogQuickSearchAdapter(this, 0, LogQuickSearch.all());
        mItem = new ArrayList<>();
        searchAdapter = new SearchAdapter(this, mItem);
        listView.setAdapter(logQuickSearchAdapter);
        listContainer.setAdapter(searchAdapter);
        set = new HashSet<>();
        mNameSearch = new SearchItem();
        SetTypeFace();
        InitiateToolbarTabs();
        InitiateSearch();
        HandleSearch();
        IsAdapterEmpty();
        db = new DatabaseHelper(this);
        new RetrieveFeedTask().execute();
    }

    private void InitiateToolbarTabs() {
        toolbar.setTitleTextColor(res.getColor(R.color.white));
        //toolbar.setNavigationIcon(R.mipmap.ic_menu);
        toolbar.inflateMenu(R.menu.menu_main);
        tabs.setTabTextColors(Color.parseColor("#FFFFFFFF"), Color.parseColor("#FFFFFF"));
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        MyPagerAdapter pagerAdapterToolbarSearch = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapterToolbarSearch);
        tabs.setupWithViewPager(viewPager);
    }

    private void InitiateSearch() {
        toolbar.setNavigationIcon(null);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
            int menuItem = item.getItemId();
            switch (menuItem) {
                case R.id.action_search:
                    IsAdapterEmpty();
                    initiateSearch.handleToolBar(MainActivity.this, card_search, toolbar, view_search, listView, edit_text_search, line_divider);
                    break;
                default:
                    break;
            }
            return false;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LogQuickSearch logQuickSearch = logQuickSearchAdapter.getItem(position);
                edit_text_search.setText(logQuickSearch.getName());
                listView.setVisibility(View.GONE);
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edit_text_search.getWindowToken(), 0);
                toolbar_shadow.setVisibility(View.GONE);
                searchName(logQuickSearch.getName(), 0);
            }
        });
        edit_text_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_text_search.getText().toString().length() == 0) {
                    logQuickSearchAdapter = new LogQuickSearchAdapter(MainActivity.this, 0, LogQuickSearch.all());
                    listView.setAdapter(logQuickSearchAdapter);
                    clearSearch.setImageResource(R.mipmap.ic_keyboard_voice);
                    IsAdapterEmpty();
                } else {
                    logQuickSearchAdapter = new LogQuickSearchAdapter(MainActivity.this, 0, LogQuickSearch.FilterByName(edit_text_search.getText().toString()));
                    listView.setAdapter(logQuickSearchAdapter);
                    clearSearch.setImageResource(R.mipmap.ic_close);
                    IsAdapterEmpty();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (edit_text_search.getText().toString().length() == 0) {

            } else {
                mAsyncTask.cancel(true);
                edit_text_search.setText("");
                listView.setVisibility(View.VISIBLE);
                clearItems();
                ((InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                IsAdapterEmpty();
            }
            }
        });
    }

    private void UpdateQuickSearch(String item) {
        for (int i = 0; i < logQuickSearchAdapter.getCount(); i++) {
            LogQuickSearch ls = logQuickSearchAdapter.getItem(i);
            String name = ls.getName();
            set.add(name.toUpperCase());
        }
        if (set.add(item.toUpperCase())) {
            LogQuickSearch recentLog = new LogQuickSearch();
            recentLog.setName(item);
            recentLog.setDate(new Date());
            recentLog.save();
            logQuickSearchAdapter.add(recentLog);
            logQuickSearchAdapter.notifyDataSetChanged();
        }
    }

    private void HandleSearch() {
        image_search_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            initiateSearch.handleToolBar(MainActivity.this, card_search, toolbar, view_search, listView, edit_text_search, line_divider);
            listContainer.setVisibility(View.GONE);
            toolbar.setNavigationIcon(null);
            toolbar_shadow.setVisibility(View.VISIBLE);
            clearItems();
            }
        });
        edit_text_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (edit_text_search.getText().toString().trim().length() > 0) {
                    clearItems();
                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edit_text_search.getWindowToken(), 0);
                    UpdateQuickSearch(edit_text_search.getText().toString());
                    listView.setVisibility(View.GONE);
                    searchName(edit_text_search.getText().toString(), 0);
                    toolbar_shadow.setVisibility(View.GONE);
                }
                return true;
            }
            return false;
            }
        });
    }

    private void IsAdapterEmpty() {
        if (logQuickSearchAdapter.getCount() == 0) {
            line_divider.setVisibility(View.GONE);
        } else {
            line_divider.setVisibility(View.VISIBLE);
        }
    }

    private void SetTypeFace() {
        Typeface roboto_regular = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        edit_text_search.setTypeface(roboto_regular);
    }

    /**
     * Handle Search
     */
    private void clearItems() {
        listContainer.setVisibility(View.GONE);
        mItem.clear();
        searchAdapter.notifyDataSetChanged();
    }

    private void searchName(final String item, final int page_num) {
        mAsyncTask = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                marker_progress.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... arg0) {
                JSONArray name = mNameSearch.searchName(item, page_num);
                JSONArray NAMES_ARRAY = name;
                try {
                    if (name != null) {
                        if (NAMES_ARRAY != null) {
                            for (int i = 0; i < NAMES_ARRAY.length(); i++) {
                                JSONObject _items = NAMES_ARRAY.optJSONObject(i);
                                Log.e("FOod", _items.toString());
                                String mom_name = _items.getString("name");
                                String mom_address = _items.getString("anschrift");
                                mItem.add(new Item(mom_name, mom_address));
                            }
                        }
                    }
                } catch (JSONException exception) {
                    exception.printStackTrace();
                    return "Error";
                }
                return "";
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                marker_progress.setVisibility(View.GONE);
                searchAdapter.notifyDataSetChanged();
                if (mItem.size() > 0) {
                    toolbar_shadow.setVisibility(View.GONE);
                    TranslateAnimation slide = new TranslateAnimation(0, 0, listContainer.getHeight(), 0);
                    slide.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            listContainer.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    slide.setDuration(300);
                    listContainer.startAnimation(slide);
                    listContainer.setVerticalScrollbarPosition(0);
                    listContainer.setSelection(0);
                } else {
                    toolbar_shadow.setVisibility(View.VISIBLE);
                    listContainer.setVisibility(View.GONE);
                }
            }

            @Override
            protected void onCancelled() {
                marker_progress.setVisibility(View.GONE);
            }

        };
        mAsyncTask.execute();
    }

    class RetrieveFeedTask extends AsyncTask<String, Void, JSONArray> {
        private Exception exception;
        protected JSONArray doInBackground(String... urls) {
            try {
                JSONObject json = new JSONObject(getJSON("https://www.berlin.de/ba-steglitz-zehlendorf/politik-und-verwaltung/" +
                        "aemter/jugendamt/angebote-fuer-kinder-jugendliche-und-familien/" +
                        "kinderbetreuung/tagespflegestellen/index.php/index/all.json?q="));
                JSONArray array = json.getJSONArray("index");
                return array;
            } catch (Exception e) {
                this.exception = e;

                return null;
            }
        }

        protected void onPostExecute(JSONArray array) {
            for(int i = 0; i < array.length(); i++){
                try {
                    JSONObject jsonObject = array.getJSONObject(i);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelper.SPALTE_1, jsonObject.getInt("id"));
                    contentValues.put(DatabaseHelper.SPALTE_2, jsonObject.getString("name"));
                    contentValues.put(DatabaseHelper.SPALTE_3, jsonObject.getInt("anzahl_der_plaetze"));
                    contentValues.put(DatabaseHelper.SPALTE_4, jsonObject.getString("anschrift"));
                    contentValues.put(DatabaseHelper.SPALTE_5, jsonObject.getString("telefon"));
                    contentValues.put(DatabaseHelper.SPALTE_6, jsonObject.getDouble("breitengrad"));
                    contentValues.put(DatabaseHelper.SPALTE_7, jsonObject.getDouble("laengengrad"));
                    contentValues.put(DatabaseHelper.SPALTE_8, false);

                    Cursor cursor = getContentResolver().query(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, jsonObject.getInt("id")), new String[]{DatabaseHelper.SPALTE_1, DatabaseHelper.SPALTE_8}, null, null, null);
                    if(cursor.getCount() <=0 ){
                        cursor.close();
                        getContentResolver().insert(MyContentProvider.CONTENT_URI1, contentValues);
                    }
                    else{
                        Boolean favorite = false;
                        if (cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                favorite = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SPALTE_8)) > 0;
                            }
                        }
                        cursor.close();
                        contentValues.put(DatabaseHelper.SPALTE_8, favorite);
                        getContentResolver().update(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI1, jsonObject.getInt("id")), contentValues, null, null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getJSON(String url) {
        HttpsURLConnection con = null;
        try {
            URL u = new URL(url);
            con = (HttpsURLConnection) u.openConnection();
            con.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
}
