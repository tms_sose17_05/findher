package com.tms.findher.Fragments;

import com.tms.findher.MyPagerAdapter;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by burak on 21.07.2017.
 */
@SuppressWarnings("DefaultFileTemplate")
public class FragmentListTest {
    @Test
    public void getClassCompatibility() throws Exception {
        FragmentList fragmentList = new FragmentList();
        assertSame(FragmentList.class, fragmentList.getClass());
    }

}