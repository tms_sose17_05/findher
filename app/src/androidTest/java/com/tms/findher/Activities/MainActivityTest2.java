package com.tms.findher.Activities;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by burak on 07.07.2017.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest2 {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.tms.findher", appContext.getPackageName());
    }
}