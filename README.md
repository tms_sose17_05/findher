#Find Her

#Vorwort
Eine großes Problem heutzutage ist die Kinderbetreuung. Welche Möglichkeiten gibt
es und wo finde ich für mich als Elternteil eine zu mir passende Tagesmutter. Dabei
kam uns die Idee zu der App Find Her. Es gibt keine App die eine Übersicht über
aktuell verfügbare und vor allem auch qualifizierte Tagesmütter gibt. Welches
Elternteil wünscht sich nicht, eine einfache Übersicht über die Möglichkeiten der
Kinderbetreuung?

#Beschreibung der Idee
Grundsätzlich geht es um die möglichst einfache Darstellung der Standorte von
Tagesmüttern. Deshalb möchten wir zwei Arten von Darstellungen anbieten.
Einmal die Ansicht auf der Karte, mit einer Anzeige des Standortes. Beim anklicken
eines Standortes sollen alle Informationen zu der jeweiligen Tagesmutter angezeigt
werden. Außerdem kann man hier die Tagesmutter zur Favoritenliste hinzufügen
oder auch direkt anrufen.
Die zweite Ansicht, ist eine einfach gestaltete Listenansicht. Dabei werden
Tabellarisch alle Informationen angezeigt. Natürlich muss eine Sortier- oder
Suchfunktion bereitgestellt werden. Dies erleichtert dann z.B. die Suche in Bezirken,
oder anhand von Postleitzahlen. Auch weitere Möglichkeiten sollen später
bereitgestellt werden.
In einer dritten Ansicht werden die eigenen Favoriten unter den Tagesmüttern
angezeigt. So kann man schneller einen Überblick über Tagesmütter erhalten, mit
denen man bereits gute Erfahrungen gemacht hat.

#Zukünftige Erweiterungen
Ein Späteres Ziel ist es, dass die Lokal eingetragene Tagesmütter mit einem Server
abgeglichen werden, und entsprechend auf die verschiedenen Nutzer verteilt wird.
Das ermöglicht die Nutzergesteuerte Erweiterung der Datensätze.
Des weiteren soll es für beide Ansichten eine Sortierfunktion für verschiedene
Ansätze geben. Beispiel Plätze, Postleitzahl usw..
Eine weitere Idee ist ein Bewertungssystem für die Tagesmütter über das man die
Meinungen anderer Nutzer zur Rate ziehen kann.
Außerdem ist es möglich die App um Kindergärten oder andere
Betreuungsmöglichkeiten zu erweitern.